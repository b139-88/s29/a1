const express = require('express');
const PORT = process.env.PORT || 8888;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let users = [
    {
        username: 'johndoe',
        password: 'johndoe1234',
    }
];

app.get('/home', (req, res) => {
    res.send('Welcome to the homepage!');
});

app.get('/users', (req, res) => {
    res.send(users);
});

app.delete('/delete-user', (req, res) => {
    users.pop(req.body.username);
    res.send(`User ${req.body.username} has been deleted.`);
    console.log(users);
});

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));